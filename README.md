# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

I created the MarketSimilator because I was researching the stock market and began to theorize about an optimal
investment strategy to avoid downturns in the market (not necessary just maximizing return). After doing some research
I learned that DCA (Dollar Cost Averaging) is a theoretic strategy to avoid the need to "time the market." I decided to
create a MarketSimilator to evaluate whether a DCA approach generally performs better for different investment horizons.

The MarketSimilator is a work in progress. It's been put on hold because of other projects demanding my time but it's a
fun academic excercise to evaluate investment strategies over historical data.

### How do I get set up? ###

* TODO - create setup guide

### Contribution guidelines ###

* Talk to me

### Who do I talk to? ###

* aet006@bucknell.edu