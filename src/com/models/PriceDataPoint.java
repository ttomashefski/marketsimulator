package com.models;

import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PriceDataPoint {
	private static final Logger LOGGER = LogManager.getLogger(PriceDataPoint.class);
	private int mOpenPrice; //10.00 == 1000
	private Date mDate;
	private PriceDataPoint mNext;
	//private 
	
	public PriceDataPoint(Date date, int openPrice) {
		mDate = date;
		mOpenPrice = openPrice;
	}
	
	public void setNext(PriceDataPoint next) {
		mNext = next;
	}
	
	public PriceDataPoint getNext() {
		return mNext;
	}
	
	public boolean hasNext() {
		return mNext != null;
	}
	
	public Date getDate() {
		return mDate;
	}

	public int getOpenPrice() {
		return mOpenPrice;
	}
	
	@Override
	public String toString() {
		return "[PricePoint: " + mOpenPrice + ", " +  mDate + "]";
	}
	
	@Override
	public boolean equals(Object other) {
		PriceDataPoint priceDataPoint = (PriceDataPoint) other;
		Date otherDate = priceDataPoint.getDate();
		return isLastTradingDateBefore(otherDate);
	}
	
	public boolean isLastTradingDateBefore(Date otherDate) {
		if (mNext == null) {
			return true;
		} else if (mNext.getDate() == null) {
			LOGGER.error("NULL Date for " + mNext);
			return true;
		} else if (getDate().compareTo(otherDate) <= 0 
				&& mNext.getDate().compareTo(otherDate) > 0) {
			LOGGER.trace("Date Comparison Success: " + getDate() + " is before " + otherDate);
			LOGGER.trace("Date Comparison Success: " + mNext.getDate() + " is after " + otherDate);
			return true;
		}
		LOGGER.trace("Date Comparison No Success: " + getDate() + " is not before " + otherDate);
		LOGGER.trace("Date Comparison No Success: " + mNext.getDate() + " is not after " + otherDate);
		return false;
	}

}
