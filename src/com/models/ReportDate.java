package com.models;

import java.util.Date;

public class ReportDate {
	private Date mDate;
	private int mHorizonInMonths;
	
	public ReportDate(Date date, int horizonInMonths) {
		mDate = new Date(date.getTime());
		mHorizonInMonths = horizonInMonths;
	}
	
	public Date getDate() {
		return mDate;
	}
	
	public int getTimeHorizon() {
		return mHorizonInMonths;
	}
	
	@Override
	public String toString() {
		return "[ReportDate: " + mDate + ", " + mHorizonInMonths + "]";
	}

}
