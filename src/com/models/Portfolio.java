package com.models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Portfolio {
	private static final Logger LOGGER = LogManager.getLogger(Portfolio.class);
	int mInitialValue;
	int mCash; //$0.01 is 1
	int mEquity;

	public Portfolio(int portfolioValue) {
		mInitialValue = portfolioValue;
		mCash = portfolioValue; //assuming dollar value $0.01 is 1
		mEquity = 0;
	}
	
	public int getTotalValue() {
		return mCash + mEquity;
	}
	
	public int getInitialValue() {
		return mInitialValue;
	}

	public int getCash() {
		return mCash;
	}

	public void addCash(int cash) {
		this.mCash += cash;
	}
	
	public void removeCash(int cash) {
		this.mCash -= cash;
	}

	public int getEquity() {
		return mEquity;
	}

	public void addEquity(int equity) {
		this.mEquity += equity;
	}
	
	public void removeEquity(int equity) {
		this.mEquity -= equity;
		if (mEquity < 0) {
			LOGGER.debug("EQUITY LESS THAN 0!!!!!");
		}
	}

	public String toString() {
		return "Total: " + ((mCash + mEquity) / 100) + " Cash: " + (mCash / 100) + " Equity: " + (mEquity / 100);
	}
	
}
