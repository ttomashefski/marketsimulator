package com.models.brokerfactory;

import java.util.Map;
import java.util.Stack;

import com.models.ReportDate;
import com.models.broker.IBroker;
import com.models.broker.OpenPriceTriggerCWDCABroker;

public class OpenPriceTriggerCWDCABrokerFactory extends BrokerFactoryBase {
	
	private Map<String, Double> mCapeRatioMap;
	private int mBuyTrigger;
	private int mSellTrigger;
	private int mPortfolioValue;
	
	public OpenPriceTriggerCWDCABrokerFactory(int portfolioValue, Stack<ReportDate> reportDates, int buyTrigger, int sellTrigger, 
			Map<String, Double> capeRatioMap) {
		super(reportDates);
		mCapeRatioMap = capeRatioMap;
		mBuyTrigger = buyTrigger;
		mSellTrigger = sellTrigger;
		mPortfolioValue = portfolioValue;
	}

	@Override
	public IBroker createBroker() throws IBrokerFactoryException {
		return new OpenPriceTriggerCWDCABroker(mPortfolioValue, mInvestmentReportingDates, mBuyTrigger, mSellTrigger, mCapeRatioMap);
	}

}
