package com.models.brokerfactory;

import java.util.Stack;

import com.models.ReportDate;
import com.models.broker.IBroker;

public class BrokerFactoryBase implements IBrokerFactory {
	protected Stack<ReportDate> mInvestmentReportingDates;
	
	protected BrokerFactoryBase(Stack<ReportDate> investmentReportingDates) {
		mInvestmentReportingDates = investmentReportingDates;
	}

	@Override
	public IBroker createBroker() throws IBrokerFactoryException {
		throw new IBrokerFactoryException("BrokerFactoryBase unimplemented createBroker");
	}
}
