package com.models.brokerfactory;

import com.models.broker.IBroker;

public interface IBrokerFactory {
	public IBroker createBroker() throws IBrokerFactoryException;
}
