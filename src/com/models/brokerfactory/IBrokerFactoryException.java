package com.models.brokerfactory;

public class IBrokerFactoryException extends Exception {
	private static final long serialVersionUID = -1219718070196412273L;
	
	public IBrokerFactoryException(String string) {
		super(string);
	}

}