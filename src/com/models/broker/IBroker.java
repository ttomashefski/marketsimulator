package com.models.broker;

import com.models.PriceDataPoint;

public interface IBroker {	
	public void setCurrentPricePoint(PriceDataPoint currentPricePoint) throws IBrokerException;
}
