package com.models.broker;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PurchaseHistory {
	private static final Logger LOGGER = LogManager.getLogger(PurchaseHistory.class);

	protected Stack<Integer> mPurchaseHistory;
	protected Queue<Integer> mPurchaseHistoryQueue;
	protected PurchaseTrackingStrategy mStrategy;
	
	PurchaseHistory(PurchaseTrackingStrategy strategy) {
		mPurchaseHistory = new Stack<Integer>();
		mPurchaseHistoryQueue = new PriorityQueue<Integer>(50); //default prioritized lowest number first in queue
		mStrategy = PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING;
	}

	public boolean isEmpty() {
		boolean isEmpty = true;
		if (mStrategy == PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING) {
			isEmpty = mPurchaseHistoryQueue.isEmpty();
		} else if (mStrategy == PurchaseTrackingStrategy.STACK_PRICE_TRACKING) {
			isEmpty = mPurchaseHistory.isEmpty();
		} else {
			LOGGER.error("Invalid purchase tracking strategy!");
		}
		return isEmpty;
	}

	public int peek() {
		int price = 0;		
		if (mStrategy == PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING) {
			price = mPurchaseHistoryQueue.peek();
		} else if (mStrategy == PurchaseTrackingStrategy.STACK_PRICE_TRACKING) {
			price = mPurchaseHistory.peek();
		} else {
			LOGGER.error("Invalid purchase tracking strategy!");
		}
		return price;
	}

	public int pop() {
		int price = 0;		
		if (mStrategy == PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING) {
			price = mPurchaseHistoryQueue.poll();
		} else if (mStrategy == PurchaseTrackingStrategy.STACK_PRICE_TRACKING) {
			price = mPurchaseHistory.pop();
		} else {
			LOGGER.error("Invalid purchase tracking strategy!");
		}
		return price;
	}

	public void push(int openPrice) {
		if (mStrategy == PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING) {
			mPurchaseHistoryQueue.add(openPrice);
		} else if (mStrategy == PurchaseTrackingStrategy.STACK_PRICE_TRACKING) {
			mPurchaseHistory.push(openPrice);
		} else {
			LOGGER.error("Invalid purchase tracking strategy!");
		}
	}

	public int size() {
		int size = 0;		
		if (mStrategy == PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING) {
			size = mPurchaseHistoryQueue.size();
		} else if (mStrategy == PurchaseTrackingStrategy.STACK_PRICE_TRACKING) {
			size = mPurchaseHistory.size();
		} else {
			LOGGER.error("Invalid purchase tracking strategy!");
		}
		return size;
	}
}
