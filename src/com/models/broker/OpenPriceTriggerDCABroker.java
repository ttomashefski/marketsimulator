package com.models.broker;

import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.models.ReportDate;

public class OpenPriceTriggerDCABroker extends BrokerBase {
	private static final Logger LOGGER = LogManager.getLogger(OpenPriceTriggerDCABroker.class);
	
	protected int mPercentageBuyTriggerPoint; //-0.01 represented as -1 <- remember we buy when price goes down
	protected int mPercentageSellTriggerPoint; //0.01 represented as 1
		
	
	public OpenPriceTriggerDCABroker(int portfolioValue, Stack<ReportDate> reportingDates, int percentageBuyTrigger, int percentageSellTrigger) {
		super(portfolioValue, reportingDates);
		mPercentageBuyTriggerPoint = percentageBuyTrigger;
		mPercentageSellTriggerPoint = percentageSellTrigger;
	}
	
	protected boolean checkOpenPriceTriggerSellCondition(int percentChange) {
		return percentChange >= mPercentageSellTriggerPoint;
	}

	protected boolean checkOpenPriceTriggerBuyCondition(int percentChange) {
		return percentChange <= mPercentageBuyTriggerPoint;
	}
	
	@Override
	protected void executeInvestmentStrategy() {
		if (mCurrentPricePoint == null || mPreviousPricePoint == null) {
			LOGGER.warn("Unable to execute investment strategy due to null price point. "
					+ "Previous: " + mPreviousPricePoint
					+ " Current: " + mCurrentPricePoint);
			return;
		}
		int percentChange = calculateDailyBasisPointChange();
		//System.out.println("Percent Change: " + percentChange);
		int quantity = 50;
		
		if (checkOpenPriceTriggerSellCondition(percentChange) && checkSellCondition()) { //SELL when high
			// For now we'll just sell 1 share
			sellAtCurrentPricePoint(quantity);
			
		} else if (checkOpenPriceTriggerBuyCondition(percentChange) && checkBuyCondition(quantity)) { //BUY when low
			// For now we'll just buy 1 share
			buyAtCurrentPricePoint(quantity);
		}
	}

}
