package com.models.broker;

import java.util.Calendar;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.models.Portfolio;
import com.models.PriceDataPoint;
import com.models.ReportDate;
import com.models.brokerfactory.BrokerStrategy;
import com.reporting.MarketType;
import com.reporting.PerformanceReport;
import com.simulation.SimulationTracker;
import com.simulation.data.Database;

/**
 * BrokerBase defines the base class implementation for a stock broker. Brokers manage a portfolio and
 * execute trades based on an underlying strategy defined in executeInvestmentStrategy()
 * 
 * @author Tony
 */
public class BrokerBase implements IBroker {
	private static final Logger LOGGER = LogManager.getLogger(BrokerBase.class);
	
	protected Portfolio mPortfolio;
	protected PurchaseHistory mPurchaseHistory;
	
	protected Stack<ReportDate> mReportingDates;
	protected PriceDataPoint mCurrentPricePoint;
	protected PriceDataPoint mPreviousPricePoint;	
	protected PriceDataPoint mStartingPricePoint;
	protected String mStartingDateKey;
	protected BrokerStrategy mStrategy;
	protected int mSharesHeld;
	
	public BrokerBase(int portfolioValue, Stack<ReportDate> reportingDates) {
		mPortfolio = new Portfolio(portfolioValue);
		mPurchaseHistory = new PurchaseHistory(PurchaseTrackingStrategy.QUEUE_PRICE_TRACKING);
		mReportingDates = new Stack<ReportDate>();
		mReportingDates.addAll(reportingDates);
		mSharesHeld = 0;

//		LOGGER.debug("Reporting dates in base: ");
//		while (!mReportingDates.isEmpty()) {
//			LOGGER.debug("" + mReportingDates.pop());
//		}
	}
	
	protected boolean checkSellCondition() {
		return !mPurchaseHistory.isEmpty()
				&& mPurchaseHistory.peek() < mCurrentPricePoint.getOpenPrice();
	}

	protected void sellAtCurrentPricePoint(int quantity) {
		LOGGER.trace("Executing Sale of: " + quantity + " at " + mCurrentPricePoint.getOpenPrice() + " on " + mCurrentPricePoint.getDate());
		mPortfolio.addCash(mCurrentPricePoint.getOpenPrice() * quantity);
		mPortfolio.removeEquity(mPurchaseHistory.pop() * quantity);
		mSharesHeld -= quantity;
		LOGGER.trace("Portolio: " + mPortfolio);
	}

	protected boolean checkBuyCondition(int quantity) {
		return mPortfolio.getCash() > (mCurrentPricePoint.getOpenPrice() * quantity);
	}

	protected void buyAtCurrentPricePoint(int quantity) {
		LOGGER.trace("Executing Purchase of: " + quantity + " at " + mCurrentPricePoint.getOpenPrice() + " on " + mCurrentPricePoint.getDate());
		mPortfolio.addEquity(mCurrentPricePoint.getOpenPrice() * quantity);
		mPortfolio.removeCash(mCurrentPricePoint.getOpenPrice() * quantity);
		mPurchaseHistory.push(mCurrentPricePoint.getOpenPrice());
		mSharesHeld += quantity;
		LOGGER.trace("Portfolio: " + mPortfolio);
	}
	
	/**
	 * Sets the current price point of the stock and the broker execute's the underlying
	 * investment strategy.
	 */
	@Override
	public void setCurrentPricePoint(PriceDataPoint currentPricePoint) throws IBrokerException {
		if (mStartingPricePoint == null) {
			mStartingPricePoint = currentPricePoint;
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentPricePoint.getDate());
			mStartingDateKey = cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.YEAR);
		}
		mPreviousPricePoint = mCurrentPricePoint;
		mCurrentPricePoint = currentPricePoint;
		executeInvestmentStrategy();
		filePerformanceReport();
	}
	
	/**
	 * Inserts a new performance report into the database
	 * @param investmentHorizonInMonths
	 * @param report
	 */
	protected void filePerformanceReport() {
		try {
			ReportDate nextReportingDate = mReportingDates == null || mReportingDates.isEmpty() ? null : mReportingDates.peek();
			if (nextReportingDate == null) {
				//LOGGER.trace("Next reporting date is null!");
			} else {
				//LOGGER.trace("Next reporting date is: " + nextReportingDate);
			}
			//System.out.println("Next reporting date: " + nextReportingDate.getDate());
			if (nextReportingDate != null && nextReportingDate.getDate() != null 
					&& mCurrentPricePoint.isLastTradingDateBefore(nextReportingDate.getDate())) {
				mReportingDates.pop();
				int investmentHorizonInMonths = nextReportingDate.getTimeHorizon();
				//LOGGER.debug("Portfolio value on: " + mCurrentPricePoint.getDate() + " : " + mPortfolio.toString());
				int marketReturn = calculateMarketReturn();
				PerformanceReport performanceReport = 
						new PerformanceReport.PerformanceReportBuilder(mStartingPricePoint.getDate(), mCurrentPricePoint.getDate())
							.marketType(MarketType.getMarketTypeFor(marketReturn))
							.marketReturn(marketReturn)
							.investmentHorizonInMonths(nextReportingDate.getTimeHorizon())
							.portfolioRealizedReturn(calculateRealizedReturn())
							.portfolioTotalReturn(calculateTotalReturn())
							.build();
				LOGGER.info("\nFiling performance report:" + performanceReport.toString());
				SimulationTracker.getInstance().incrementReportCountFor(investmentHorizonInMonths, mReportingDates.isEmpty());
				Database.PERFORMANCE_REPORT_DATABASE.insertEntry(investmentHorizonInMonths, performanceReport);		
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Executes the underlying investment strategy. MUST BE OVERRIDDEN
	 * @throws IBrokerException
	 */
	protected void executeInvestmentStrategy() throws IBrokerException {
		throw new IBrokerException(null);
	}
	
	/**
	 * 
	 * @return 100 == 1.00%
	 */
	protected int calculateDailyBasisPointChange() {
		return ((mCurrentPricePoint.getOpenPrice() - mPreviousPricePoint.getOpenPrice()) * 10000) / mPreviousPricePoint.getOpenPrice();
	}
	
	/**
	 * @return the market return in basis points 100 == 1.00%
	 */
	protected int calculateMarketReturn() {
		LOGGER.debug("Market Return calculating using current: " + mCurrentPricePoint.getOpenPrice());
		LOGGER.debug("Market Return calculating using starting: " + mStartingPricePoint.getOpenPrice());
		return (10000 * (mCurrentPricePoint.getOpenPrice() - mStartingPricePoint.getOpenPrice())) / mStartingPricePoint.getOpenPrice();
	}
	
	/**
	 * @return the underlying portfolio's return in basis points 100 == 1.00%
	 */
	protected int calculateRealizedReturn() {
		return ((mPortfolio.getCash() - mPortfolio.getInitialValue()) * 10000) / mPortfolio.getInitialValue();
	}
	
	/**
	 * TODO - might want to move this and realized return into portfolio class...
	 * @return the underlying portolio's total return in basis points 100 == 1.00%
	 */
	protected int calculateTotalReturn() {
		int outstandingEquityValue = mSharesHeld * mCurrentPricePoint.getOpenPrice();
		return ((mPortfolio.getCash() + outstandingEquityValue - mPortfolio.getInitialValue()) * 10000) / mPortfolio.getInitialValue();
	}
}
