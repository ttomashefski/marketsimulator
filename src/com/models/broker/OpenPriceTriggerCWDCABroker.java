package com.models.broker;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.models.ReportDate;

public class OpenPriceTriggerCWDCABroker extends OpenPriceTriggerDCABroker {
	private static final Logger LOGGER = LogManager.getLogger(OpenPriceTriggerCWDCABroker.class);
	private static final long TWICE_THE_SQUARE_OF_STDEV = 8845; //1 == $0.01
	private static final long HISTORICAL_AVERAGE_CAPE = 1675;
		
	private Map<String, Double> mCapeRatioMap;
	
	public OpenPriceTriggerCWDCABroker(int portfolioValue, Stack<ReportDate> reportingDates, 
			int percentageBuyTrigger, int percentageSellTrigger, Map<String, Double> capeRatioMap) {
		super(portfolioValue, reportingDates, percentageBuyTrigger, percentageSellTrigger);
		mCapeRatioMap = capeRatioMap;
	}
	
	@Override
	protected void executeInvestmentStrategy() {
		if (mCurrentPricePoint == null || mPreviousPricePoint == null) {
			LOGGER.warn("Unable to execute investment strategy due to null price point. "
					+ "Previous: " + mPreviousPricePoint
					+ " Current: " + mCurrentPricePoint);
			return;
		}
		int percentChange = calculateDailyBasisPointChange();
		//System.out.println("Percent Change: " + percentChange);
		int quantity = 1;
		Date currentDate = mCurrentPricePoint.getDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		String key = (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
		double weightFactor = mCapeRatioMap.get(key);
		//e^(-x^2/(2 * (6.75^2))) <- gaussian apodization function
		int currentPrice = mCurrentPricePoint.getOpenPrice();
		long vestedTarget = (long) (mPortfolio.getTotalValue() * weightFactor);
		long vestedDifference = vestedTarget - mPortfolio.getEquity();
		LOGGER.trace("Weighting: " + weightFactor);
		
		// Buy to increase equity to vestedTarget or sell to decrease equity to vestedTarget
		if (checkOpenPriceTriggerBuyCondition(percentChange) && vestedDifference > 0) {
			quantity = Math.abs((int) (vestedDifference / currentPrice));
		} else if (checkOpenPriceTriggerSellCondition(percentChange) && vestedDifference < 0) {
			quantity = Math.abs((int) (vestedDifference / currentPrice));
		}
		
		if (checkOpenPriceTriggerSellCondition(percentChange) && checkSellCondition()) { //SELL when high
			// For now we'll just sell 1 share
			sellAtCurrentPricePoint(quantity);
			
		} else if (checkOpenPriceTriggerBuyCondition(percentChange) && checkBuyCondition(quantity)) { //BUY when low
			// For now we'll just buy 1 share
			buyAtCurrentPricePoint(quantity);
		}
		
	}
}
