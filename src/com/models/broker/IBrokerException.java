package com.models.broker;

public class IBrokerException extends Exception {
	private static final long serialVersionUID = 2719648770198387133L;

	public IBrokerException(String string) {
		super(string);
	}
}
