package com.models.broker;

public enum PurchaseTrackingStrategy {
	QUEUE_PRICE_TRACKING,
	STACK_PRICE_TRACKING
}
