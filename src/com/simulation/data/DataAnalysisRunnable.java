package com.simulation.data;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataAnalysisRunnable implements Runnable {
	private static final Logger LOGGER = LogManager.getLogger(DataAnalysisRunnable.class);
	
	private ReturnStatistics analyze(ReportStatistics stats) {
		ReturnStatistics returnStats = new ReturnStatistics(stats.getMarketType(), stats.getTotalSimulationRuns());
		returnStats.add(new ReturnStatistic(ReturnType.REALIZED_RETURN, stats));
		returnStats.add(new ReturnStatistic(ReturnType.TOTAL_RETURN, stats));
		returnStats.add(new ReturnStatistic(ReturnType.AVERAGE_PER_TRADE_RETURN, stats));
		returnStats.add(new ReturnStatistic(ReturnType.MARKET_RETURN, stats));
		//TODO - long brokerTotalWinPercentage = 0;
		return returnStats;
		
		
	}

	@Override
	public void run() {
		LOGGER.debug("DataAnalysisRunnable started...");
		try {
			while (true) {
				// Analyzes a complete set of aggregated reports for an investment horizon
				Collection<ReportStatistics> aggregatedReports = Database.REPORT_STATISTICS_DATABASE.getNextCollectionForAnalysis(null);
				if (aggregatedReports instanceof ReportStatisticsSet) {
					ReportStatisticsSet aggregatedStatReports = (ReportStatisticsSet) aggregatedReports;
					int investmentHorizonInMonths = aggregatedStatReports.getInvestmentHorizonInMonths();
					LOGGER.debug("Running analysis on horizon: " + investmentHorizonInMonths);
					//long totalSimulationRuns = 0;
					ReturnStatisticsSet statisticsSet = new ReturnStatisticsSet(investmentHorizonInMonths);
					if (aggregatedStatReports.get(StatisticsSet.BULL_STATS) != null) {
						statisticsSet.set(StatisticsSet.BULL_STATS, analyze(aggregatedStatReports.get(StatisticsSet.BULL_STATS)));
					}
					if (aggregatedStatReports.get(StatisticsSet.BEAR_STATS) != null) {
						statisticsSet.set(StatisticsSet.BEAR_STATS, analyze(aggregatedStatReports.get(StatisticsSet.BEAR_STATS)));
					}
					statisticsSet.set(StatisticsSet.MIX_STATS, analyze(aggregatedStatReports.get(StatisticsSet.MIX_STATS)));
					
					LOGGER.debug("Inserting into final return statistic database: " + investmentHorizonInMonths 
							+ " " + statisticsSet);
					Database.FINAL_RETURN_STATISTICS_DATABASE.addToEntry(investmentHorizonInMonths, statisticsSet);
					Database.FINAL_RETURN_STATISTICS_DATABASE.addToReadyQueue(investmentHorizonInMonths);
					LOGGER.trace("Successfully added record");
				} else {
					LOGGER.error("Collection type was not ReportStatisticsSet");
				}
				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
