package com.simulation.data;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.reporting.MarketType;
import com.simulation.data.ReportStatistics.ReportStatisticsBuilder;

/**
 * Each PerformanceReportSet should include a BULL, BEAR, and MIXED market
 * for a specific investment horizon.
 * 
 * @author Tony
 */
public class ReportStatisticsSet extends StatisticsSet<ReportStatistics> {
	private static final Logger LOGGER = LogManager.getLogger(ReportStatisticsSet.class);
	private static final long serialVersionUID = -7403706664216081208L;	
	
	public ReportStatisticsSet() {
		super(SIZE);
		add(null);
		add(null);
		add(null);
		mInvestmentHorizonInMonths = -1;
	}
	
	public ReportStatisticsSet(int investmentHorizonInMonths) {
		super(SIZE);
		add(null);
		add(null);
		add(null);
		mInvestmentHorizonInMonths = investmentHorizonInMonths;
	}
	
	@Override
	public int size() {
		return get(MIX_STATS).getReports().size();
	}

	@Override
	public boolean addAll(Collection<? extends ReportStatistics> otherCollection) {
		if (otherCollection instanceof ReportStatisticsSet && (mInvestmentHorizonInMonths == -1 ||
				((ReportStatisticsSet) otherCollection).mInvestmentHorizonInMonths == mInvestmentHorizonInMonths)) {
			ReportStatisticsSet otherStats = ((ReportStatisticsSet) otherCollection);
			mInvestmentHorizonInMonths = otherStats.mInvestmentHorizonInMonths;
			for (int index = 0; index < SIZE; index++) {
				ReportStatistics thisStatistics = get(index);
				ReportStatistics otherStatistics = otherStats.get(index);
				if (otherStatistics == null) {
					LOGGER.warn("Other Stat was null for " + index + " in " + mInvestmentHorizonInMonths);
				} else if (thisStatistics == null) {
					set(index, otherStatistics);
				} else if (MIX_STATS == index || 
						thisStatistics.getMarketType() == otherStatistics.getMarketType()) {
					ReportStatisticsBuilder statisticsBuilder = new ReportStatisticsBuilder(thisStatistics);
					if (index == MIX_STATS) { statisticsBuilder.marketType(MarketType.FLAT_MARKET_OR_UNKNOWN); }
					LOGGER.debug("Index: " + index + " Simulation Count Now: " + thisStatistics.getTotalSimulationRuns()
							+ " Adding: " + otherStatistics.getTotalSimulationRuns());
					ReportStatistics stats = statisticsBuilder 
							 .addSimulationRun(otherStatistics.getTotalSimulationRuns())
							 .addMarketReturn(otherStatistics.getSumOfMarketReturn())
							 .addPortfolioRealizedReturn(otherStatistics.getSumOfRealizedReturn())
							 .addPortfolioTotalReturn(otherStatistics.getSumOfTotalReturn())
							 .addPerformanceReports(otherStatistics.getReports())
							 .build();
					LOGGER.debug("New Simulation Count: " + stats.getTotalSimulationRuns());
					set(index, stats);
				} else {
					LOGGER.error("This market type: " + thisStatistics.getMarketType() +
							" doesn't match other: " + otherStatistics.getMarketType());
				}
			}
			return true;
		}
		LOGGER.error("Collection was not of type ReportStatisticsSet: " +  (otherCollection instanceof ReportStatisticsSet)
				+ "\nor did not have matching investment horizon with: " + mInvestmentHorizonInMonths);
		return false;
	}
	
	/**
	 * Sets the stats at the appropriate indeces based on market type
	 * @param stats
	 */
	public void setStats(ReportStatistics stats) {
		if (stats.getMarketType() == MarketType.BEAR_MARKET) {
			set(marketTypeToIndex(MarketType.BEAR_MARKET), stats);
		} else if (stats.getMarketType() == MarketType.BULL_MARKET) {
			set(marketTypeToIndex(MarketType.BULL_MARKET), stats);
		} else {
			set(marketTypeToIndex(MarketType.FLAT_MARKET_OR_UNKNOWN), stats);
		}		
	}

	
}
