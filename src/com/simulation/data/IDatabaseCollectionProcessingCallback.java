package com.simulation.data;

public interface IDatabaseCollectionProcessingCallback {
	public void onProcessing();

	public boolean isOverrideEnabled();
}
