package com.simulation.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.reporting.MarketType;
import com.reporting.PerformanceReport;
import com.simulation.SimulationTracker;
import com.simulation.data.ReportStatistics.ReportStatisticsBuilder;

public class DataAggregationRunnable implements Runnable  {

	private static final Logger LOGGER = LogManager.getLogger(DataAggregationRunnable.class);
	
	private Map<String, ReportStatisticsBuilder> mBuilderMap;
	private Map<Integer, ReportStatisticsSet> mStatisticsMap;
	private boolean mLastPass;
	
	public DataAggregationRunnable() {
		mBuilderMap = new HashMap<String, ReportStatisticsBuilder>();
		mStatisticsMap = new HashMap<Integer, ReportStatisticsSet>();
		mLastPass = false;
	}
	
	private ReportStatistics buildReportStatistics(String builderKey, MarketType marketType, PerformanceReport report) {
		ReportStatisticsBuilder statisticsBuilder = null;
		if (mBuilderMap.containsKey(builderKey)) {
			statisticsBuilder = mBuilderMap.get(builderKey);
		} else {
			statisticsBuilder = new ReportStatisticsBuilder(marketType, report.getInvestmentHorizonInMonths());
			mBuilderMap.put(builderKey, statisticsBuilder);
		}
		return statisticsBuilder.addSimulationRun(1)
							 .addMarketReturn(report.getMarketReturn())
							 .addPortfolioRealizedReturn(report.getPortfolioRealizedReturn())
							 .addPortfolioTotalReturn(report.getPortfolioTotalReturn())
							 .addPerformanceReport(report)
							 .build();
	}

	@Override
	public void run() {
		try {
			LOGGER.debug("Data Aggregator Started...");
			boolean finalResultsReady = false;
			while (true) {
				boolean done = SimulationTracker.getInstance().isMarketSimulationComplete();
				LOGGER.debug("Data Aggregator Started... " + done);
				Collection<PerformanceReport> reports = Database.PERFORMANCE_REPORT_DATABASE.getNextCollectionForAnalysis(
						new IDatabaseCollectionProcessingCallback() {
							@Override
							public void onProcessing() {
								LOGGER.debug("CHECKING SIMULATIONS COMPLETE");
								mLastPass = SimulationTracker.getInstance().isMarketSimulationComplete();
								SimulationTracker.getInstance().setAggregatedResultsInProgress(true);
								
							}

							@Override
							public boolean isOverrideEnabled() {
								return SimulationTracker.getInstance().isMarketSimulationComplete();
							}
						});
				if (mLastPass && reports == null) {
					finalResultsReady = true;
				} else if (reports == null && !done) {
					LOGGER.warn("Null collection returned from PERFORMANCE_REPORT_DATABASE");
					continue;
				} else if (reports == null) {
					LOGGER.debug("Data aggregation complete");
					SimulationTracker.getInstance().allAggregatedResultsReady();
					break;
				}
				LOGGER.debug("Aggregating data for latest reports... count: " + reports.size());
				
				mBuilderMap.clear();
				mStatisticsMap.clear();
				Iterator<PerformanceReport> reportsIterator = reports.iterator();
				
				//Iterate through the available reports and compile them into ReportStatisticsSets to put in the Database
				while (reportsIterator.hasNext()) {
					PerformanceReport report = reportsIterator.next();
					MarketType marketType = report.getMarketType();
					int investmentHorizonInMonths = report.getInvestmentHorizonInMonths();
					
					//Obtain the current statistics builder for the given report
					String builderKey = marketType + "-" + investmentHorizonInMonths;
					String allMarketTypesKey = MarketType.FLAT_MARKET_OR_UNKNOWN + "-" + investmentHorizonInMonths;
					ReportStatistics allMarketStats = null;
					
					ReportStatistics stats = buildReportStatistics(builderKey, marketType, report);
					if (marketType != MarketType.FLAT_MARKET_OR_UNKNOWN) { // we need to add an extra entry in the case of bull/bear market types
						allMarketStats = buildReportStatistics(allMarketTypesKey, MarketType.FLAT_MARKET_OR_UNKNOWN, report);
					}
					
					// If we already have stats for this investment horizon we need to replace them
					// Otherwise we need to create a new statistics set for entries in this horizon
					if (mStatisticsMap.containsKey(investmentHorizonInMonths)) {
						LOGGER.trace("Found existing statistics for: " + investmentHorizonInMonths);
						ReportStatisticsSet tempStatSet = mStatisticsMap.get(investmentHorizonInMonths);
						tempStatSet.setStats(stats);
						if (allMarketStats != null) {
							tempStatSet.setStats(allMarketStats);
						}
					} else {
						LOGGER.trace("Creating new report set for horizon of: " + investmentHorizonInMonths);
						ReportStatisticsSet tempStatSet = new ReportStatisticsSet(investmentHorizonInMonths);
						tempStatSet.setStats(stats);
						if (allMarketStats != null) {
							tempStatSet.setStats(allMarketStats);
						}
						mStatisticsMap.put(investmentHorizonInMonths, tempStatSet);
					}
				}
				
				//Insert the records into the database
				for (Integer investmentHorizonInMonths : mStatisticsMap.keySet()) {
					LOGGER.debug("Inserting into statistic database: " + investmentHorizonInMonths 
							+ " " + mStatisticsMap.get(investmentHorizonInMonths));
					Database.REPORT_STATISTICS_DATABASE.addToEntry(investmentHorizonInMonths, mStatisticsMap.get(investmentHorizonInMonths));
					SimulationTracker.getInstance().aggregatedResultsReady(investmentHorizonInMonths, finalResultsReady);
					LOGGER.trace("Successfully added record");
				}
				SimulationTracker.getInstance().setAggregatedResultsInProgress(false);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
