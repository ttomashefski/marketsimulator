package com.simulation.data;

import com.reporting.MarketType;

public interface Statistics {	
	public MarketType getMarketType();
}
