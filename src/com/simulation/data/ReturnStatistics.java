package com.simulation.data;

import java.util.TreeSet;

import com.reporting.MarketType;

public class ReturnStatistics extends TreeSet<ReturnStatistic> implements Statistics {

	private static final long serialVersionUID = 1676800935427270218L;
		
	private final MarketType mMarketType;
	private final long mTotalSimulationRuns;
	
	public ReturnStatistics(MarketType marketType, long totalSimulationRuns) {
		mMarketType = marketType;
		mTotalSimulationRuns = totalSimulationRuns;
	}
	
	public long getTotalSimulationRuns() {
		return mTotalSimulationRuns;
	}

	@Override
	public MarketType getMarketType() {
		return mMarketType;
	}
	
	@Override
	public String toString() {
		String ret = "\nReturnStatistics[******"
				+ mMarketType
				+ " number of simulations: " + mTotalSimulationRuns;
		for (ReturnStatistic stat : this) {
			ret += stat.toString();
		}
		ret += "\n";
		return ret;
	}

}
