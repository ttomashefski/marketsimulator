package com.simulation.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.reporting.PerformanceReport;
import com.reporting.PerformanceReportSet;

public class Database {
	private static final Logger LOGGER = LogManager.getLogger(Database.class);
	public static InternalDatabase<Integer, PerformanceReportSet, PerformanceReport> PERFORMANCE_REPORT_DATABASE 
												= new InternalDatabase<Integer, PerformanceReportSet, PerformanceReport>(PerformanceReportSet.class);
	
	public static InternalDatabase<Integer, ReportStatisticsSet, ReportStatistics> REPORT_STATISTICS_DATABASE 
												= new InternalDatabase<Integer, ReportStatisticsSet, ReportStatistics>(ReportStatisticsSet.class);
	
	public static InternalDatabase<Integer, ReturnStatisticsSet, ReturnStatistics> FINAL_RETURN_STATISTICS_DATABASE 
												= new InternalDatabase<Integer, ReturnStatisticsSet, ReturnStatistics>(ReturnStatisticsSet.class);
	
	public static class InternalDatabase<K, V extends Collection<I>, I extends Object> {
		private Class<V> mClazz;
		private Map<K, V> mDatabase;
		private LinkedHashSet<K> mReadyForAnalysis;
		private int mQueueReadyCount;
		private static final int READY_COUNT_DISABLED = -1;
		
		InternalDatabase(Class<V> clazz) {
			mClazz = clazz;
			mDatabase = new HashMap<K, V>();
			mReadyForAnalysis = new LinkedHashSet<K>();
			mQueueReadyCount = READY_COUNT_DISABLED;
		}
		
		public void setQueueReadyCount(int numberOfItemsInQueue) {
			synchronized (mReadyForAnalysis) {
				mQueueReadyCount = numberOfItemsInQueue;
			}
		}
		
		public void notifyReadyForAnalysis() {
			synchronized (mReadyForAnalysis) {
				mReadyForAnalysis.notifyAll();
			}
		}
		
		public void addToReadyQueue(K key) {
			synchronized (mReadyForAnalysis) {
				if (mQueueReadyCount == READY_COUNT_DISABLED
						&& mDatabase.containsKey(key)) {
					mReadyForAnalysis.add(key);
					mReadyForAnalysis.notifyAll();
				} else {
					LOGGER.warn("Database does not contain key for: " + key + ", or ready count is not disabled: " + mQueueReadyCount);
				}
			}
		}
		
		public void insertEntry(K key, I item) throws InstantiationException, IllegalAccessException {
			synchronized (mReadyForAnalysis) {
				synchronized (mDatabase) {					
					if (mDatabase.containsKey(key) && mDatabase.get(key) != null) {
						mDatabase.get(key).add(item);
					} else {
						V reports = mClazz.newInstance();
						reports.add(item);
						mDatabase.put(key, reports);
					}
					if (mQueueReadyCount != READY_COUNT_DISABLED 
							&& mDatabase.get(key).size() >= mQueueReadyCount) {
						mReadyForAnalysis.add(key);
						mReadyForAnalysis.notifyAll();
					}
				}
			}
		}
		
		public void addToEntry(K key, V value) throws InstantiationException, IllegalAccessException {
			synchronized (mReadyForAnalysis) {
				synchronized (mDatabase) {
					if (mDatabase.containsKey(key) && mDatabase.get(key) != null) {
						mDatabase.get(key).addAll(value);
					} else {
						//V reports = mClazz.newInstance();
						//reports.addAll(value);
						mDatabase.put(key, value);
					}
					if (mQueueReadyCount != READY_COUNT_DISABLED 
							&& mDatabase.get(key).size() >= mQueueReadyCount) {
						mReadyForAnalysis.add(key);
						mReadyForAnalysis.notifyAll();
					}
				}
			}
		}
		
		public Collection<I> getNextCollectionForAnalysis(IDatabaseCollectionProcessingCallback databaseCollectionProcessingCallback) 
				throws InterruptedException {
			synchronized (mReadyForAnalysis) {
				while (mReadyForAnalysis.isEmpty() 
						&& (databaseCollectionProcessingCallback == null || !databaseCollectionProcessingCallback.isOverrideEnabled())) {
					LOGGER.debug("mReadyForAnalysis is empty");
					mReadyForAnalysis.wait();
				}
				Collection<I> result = null;
				Iterator<K> iter = mReadyForAnalysis.iterator();
				if (iter.hasNext()) {
					K key = iter.next();
					iter.remove();
					synchronized (mDatabase) {
						if (mDatabase.containsKey(key)) {
							if (databaseCollectionProcessingCallback != null) {
								databaseCollectionProcessingCallback.onProcessing();
							}
							LOGGER.debug("READING");
							result = mDatabase.get(key);
							LOGGER.debug("DONE READING");
							mDatabase.put(key, null); //to avoid returning the same results later
						} else {
							LOGGER.warn("getNextCollectionForAnalysis Database does not contain key: " + key);
						}
					}
				} else {
					LOGGER.trace("No reports ready for analysis");
				}
				return result;
			}
		}
	}

}
