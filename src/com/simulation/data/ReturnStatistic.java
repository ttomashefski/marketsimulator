package com.simulation.data;

import java.util.Iterator;
import java.util.List;

import com.reporting.MarketType;
import com.reporting.PerformanceReport;

public class ReturnStatistic implements Comparable<ReturnStatistic> {
	MarketType mMarketType;
	ReturnType mReturnType;
	long mAverageReturn;
	long mStandardDeviation;
	
	public ReturnStatistic(ReturnType returnType, ReportStatistics stats) {
		mMarketType = stats.getMarketType();
		mReturnType = returnType;
		calculateAverageReturn(stats.getSumOfReturnsForType(mReturnType), stats.getTotalSimulationRuns());
		calculateStandardDeviation(stats.getReports());
	}
	
	private void calculateAverageReturn(long sumOfReturns, long numberOfRuns) {
		numberOfRuns = numberOfRuns == 0 ? 1 : numberOfRuns;
		mAverageReturn = sumOfReturns / numberOfRuns;
	}
	
	private void calculateStandardDeviation(List<PerformanceReport> reports) {
		long sum = 0, n = 0;
		Iterator<PerformanceReport> iterator = reports.iterator();
		while (iterator.hasNext()) {
			PerformanceReport report = iterator.next();
			if (report.getMarketType() == mMarketType || mMarketType == MarketType.FLAT_MARKET_OR_UNKNOWN) {
				long diff = report.getReturnForType(mReturnType) - mAverageReturn;
				long diffSquared = diff * diff;
				sum += diffSquared;
				n += 1;
			}
		}
		n = n == 0 ? 1: n;
		mStandardDeviation = (long) Math.sqrt(sum / n);
	}
	
	@Override
	public String toString() {
		return "\nReturnStatistic[******"
				+ mReturnType
				+ " of " + mAverageReturn
				+ " +/- " + mStandardDeviation;
	}

	@Override
	public int compareTo(ReturnStatistic o) {
		return mReturnType.compareTo(o.mReturnType);
	}
	
}
