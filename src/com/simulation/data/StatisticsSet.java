package com.simulation.data;

import java.util.ArrayList;

import com.reporting.MarketType;

public abstract class StatisticsSet<T extends Statistics> extends ArrayList<T> implements Comparable<StatisticsSet<T>> {
	private static final long serialVersionUID = -7403706664216081208L;
	
	protected static final int SIZE = 3;
	public static final int BULL_STATS = 0;
	public static final int BEAR_STATS = 1;
	public static final int MIX_STATS = 2;
	
	protected int mInvestmentHorizonInMonths;
	
	public static int marketTypeToIndex(MarketType type) {		
		switch(type) {
		case BULL_MARKET:
			return BULL_STATS;
		case BEAR_MARKET:
			return BEAR_STATS;
		default:
			break;
		}
		return MIX_STATS;
	}
	
	public StatisticsSet() {
		super(SIZE);
		add(null);
		add(null);
		add(null);
		mInvestmentHorizonInMonths = -1;
	}
	
	public StatisticsSet(int investmentHorizonInMonths) {
		super(SIZE);
		add(null);
		add(null);
		add(null);
		mInvestmentHorizonInMonths = investmentHorizonInMonths;
	}
	
	public int getInvestmentHorizonInMonths() {
		return mInvestmentHorizonInMonths;
	}

	@Override
	public String toString() {
		String string = "**Horizon: " + mInvestmentHorizonInMonths + "**\n";
		for (int index = 0; index < SIZE; index++) {
			string += "" + get(index);
		}
		return string;
	}

	@Override
	public int compareTo(StatisticsSet<T> o) {
		return Integer.compare(mInvestmentHorizonInMonths, o.mInvestmentHorizonInMonths);
	}
}
