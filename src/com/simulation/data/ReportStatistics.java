package com.simulation.data;

import java.util.ArrayList;
import java.util.List;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import com.reporting.MarketType;
import com.reporting.PerformanceReport;

public class ReportStatistics implements Statistics, Comparable<ReportStatistics> {
	//private static final Logger LOGGER = LogManager.getLogger(ReportStatistics.class);
	
	private MarketType mMarketType;
	private final long mInvestmentHorizonInMonths;
	private long mTotalSimulationRuns;
	private long mSumOfRealizedReturn;
	private long mSumOfTotalReturn;
	private long mSumOfAverageReturnPerTrade;
	private long mSumOfMarketReturn;
	private final List<PerformanceReport> mReports;
	
	private ReportStatistics(ReportStatisticsBuilder builder) {
		mMarketType = builder.mMarketType;
		mInvestmentHorizonInMonths = builder.mInvestmentHorizonInMonths;
		mTotalSimulationRuns = builder.mTotalSimulationRuns;
		mSumOfRealizedReturn = builder.mSumOfRealizedReturn;
		mSumOfTotalReturn = builder.mSumOfTotalReturn;
		mSumOfAverageReturnPerTrade = builder.mSumOfAverageReturnPerTrade;
		mSumOfMarketReturn = builder.mSumOfMarketReturn;
		mReports = builder.mReports;
	}
	public MarketType getMarketType() {
		return mMarketType;
	}
	public long getHorizonInMonths() {
		return mInvestmentHorizonInMonths;
	}
	public long getTotalSimulationRuns() {
		return mTotalSimulationRuns;
	}
	public long getSumOfRealizedReturn() {
		return mSumOfRealizedReturn;
	}
	public long getSumOfTotalReturn() {
		return mSumOfTotalReturn;
	}
	public long getSumOfAverageReturnPerTrade() {
		return mSumOfAverageReturnPerTrade;
	}
	public long getSumOfMarketReturn() {
		return mSumOfMarketReturn;
	}
	public List<PerformanceReport> getReports() {
		return mReports;
	}
	
	public long getSumOfReturnsForType(ReturnType type) {
		switch (type) {
		case MARKET_RETURN:
			return mSumOfMarketReturn;
		case REALIZED_RETURN:
			return mSumOfRealizedReturn;
		case TOTAL_RETURN:
			return mSumOfTotalReturn;
		case AVERAGE_PER_TRADE_RETURN: //TODO
		default:
			return 0;
		}
	}
	
	@Override
	public String toString() {
		return "\nStatistics[******"
				+ "\n\tNumber Of Simulations: " + mTotalSimulationRuns
				+ "\n\tHorizon: " + mInvestmentHorizonInMonths
				+ "\n\tSimCount: " + mTotalSimulationRuns
				+ "\n\tMarketType: " + mMarketType
				+ "\n\tSumOfRealizedReturn: " + mSumOfRealizedReturn
				+ "\n\tSumOfTotalReturn: " + mSumOfTotalReturn
				+ "\n\tSumOfMarketReturn: " + mSumOfMarketReturn;
	}
	
	@Override
	public int compareTo(ReportStatistics other) {
		return mMarketType.compareTo(other.mMarketType);
	}

//	public boolean overwrite(ReportStatistics otherStatistics) {
//		if (mInvestmentHorizonInMonths == otherStatistics.mInvestmentHorizonInMonths) {
//			if (mMarketType != MarketType.FLAT_MARKET_OR_UNKNOWN && 
//					mMarketType != otherStatistics.mMarketType) {
//				LOGGER.warn("Converting performance report market type to " + MarketType.FLAT_MARKET_OR_UNKNOWN);
//				mMarketType = MarketType.FLAT_MARKET_OR_UNKNOWN;
//			}
//			mTotalSimulationRuns = otherStatistics.mTotalSimulationRuns;
//			mSumOfRealizedReturn = otherStatistics.mSumOfRealizedReturn;
//			mSumOfTotalReturn = otherStatistics.mSumOfTotalReturn;
//			mSumOfAverageReturnPerTrade = otherStatistics.mSumOfAverageReturnPerTrade;
//			mSumOfMarketReturn = otherStatistics.mSumOfMarketReturn;
//			return mReports.addAll(otherStatistics.mReports);
//		} else {
//			LOGGER.error("Attempted to add ReportStatistics to the wrong investment horizon!");
//		}
//		return false;
//	}
	
	/**
	 * @author Tony
	 *
	 */
	public static class ReportStatisticsBuilder {
		private long mTotalSimulationRuns;
		private MarketType mMarketType;
		private long mInvestmentHorizonInMonths;
		private long mSumOfMarketReturn;
		private long mSumOfRealizedReturn;
		private long mSumOfTotalReturn;
		private long mSumOfAverageReturnPerTrade;
		private List<PerformanceReport> mReports;

	    public ReportStatisticsBuilder(MarketType marketType, long mHorizonInMonths) {
			mTotalSimulationRuns = 0;
			mMarketType = marketType;
			mInvestmentHorizonInMonths = mHorizonInMonths;
			mSumOfRealizedReturn = 0;
			mSumOfTotalReturn = 0;
			mSumOfAverageReturnPerTrade = 0;
			mSumOfMarketReturn = 0;
	    	mReports = new ArrayList<PerformanceReport>();
	    }
	    
	    public ReportStatisticsBuilder(ReportStatistics previousReport) {
			mTotalSimulationRuns = previousReport.mTotalSimulationRuns;
			mMarketType = previousReport.mMarketType;
			mInvestmentHorizonInMonths = previousReport.mInvestmentHorizonInMonths;
			mSumOfRealizedReturn = previousReport.mSumOfRealizedReturn;
			mSumOfTotalReturn = previousReport.mSumOfTotalReturn;
			mSumOfAverageReturnPerTrade = previousReport.mSumOfAverageReturnPerTrade;
			mSumOfMarketReturn = previousReport.mSumOfMarketReturn;
	    	mReports = new ArrayList<PerformanceReport>();
	    	mReports.addAll(previousReport.mReports); //should I deep copy?
	    }
	    
	    public ReportStatisticsBuilder addSimulationRun(long simulationRun) {
	    	this.mTotalSimulationRuns += simulationRun;
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder marketType(MarketType marketType) {
	    	this.mMarketType = marketType;
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder investmentHorizonInMonths(int investmentHorizonInMonths) {
	    	this.mInvestmentHorizonInMonths = investmentHorizonInMonths;
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder addMarketReturn(long sumOfMarketReturn) {
	    	this.mSumOfMarketReturn += sumOfMarketReturn;
	    	return this;
	    }

		public ReportStatisticsBuilder addPortfolioRealizedReturn(long portfolioRealizedReturn) {
	    	this.mSumOfRealizedReturn += portfolioRealizedReturn;
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder addPortfolioTotalReturn(long portfolioTotalReturn) {
	    	this.mSumOfTotalReturn += portfolioTotalReturn;
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder addAverageReturnPerTrade(long averageReturnPerTrade) {
	    	this.mSumOfAverageReturnPerTrade += averageReturnPerTrade;
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder addPerformanceReport(PerformanceReport performanceReport) {
	    	this.mReports.add(performanceReport);
	    	return this;
	    }
	    
	    public ReportStatisticsBuilder addPerformanceReports(List<PerformanceReport> reports) {
	    	this.mReports.addAll(reports);
			return this;
		}

	    public ReportStatistics build() {
	    	return new ReportStatistics(this); //TODO - throw exception if incomplete?
	    }
		
	}
	
	
}
