package com.simulation.data;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReturnStatisticsSet extends StatisticsSet<ReturnStatistics> {
	private static final long serialVersionUID = 3639591777494313789L;
	private static final Logger LOGGER = LogManager.getLogger(ReturnStatisticsSet.class);
	
	public ReturnStatisticsSet() {
		super(SIZE);
		add(null);
		add(null);
		add(null);
		mInvestmentHorizonInMonths = -1;
	}
	
	public ReturnStatisticsSet(int investmentHorizonInMonths) {
		super(SIZE);
		add(null);
		add(null);
		add(null);
		mInvestmentHorizonInMonths = investmentHorizonInMonths;
	}
	
	@Override
	public boolean addAll(Collection<? extends ReturnStatistics> otherCollection) {
		if (otherCollection instanceof ReturnStatisticsSet && (mInvestmentHorizonInMonths == -1 ||
				((ReturnStatisticsSet) otherCollection).mInvestmentHorizonInMonths == mInvestmentHorizonInMonths)) {
			ReturnStatisticsSet otherStats = ((ReturnStatisticsSet) otherCollection);
			LOGGER.debug("Setting Horizon for " + this.getClass() + " to " + otherStats.mInvestmentHorizonInMonths);
			mInvestmentHorizonInMonths = otherStats.mInvestmentHorizonInMonths;
			for (int index = 0; index < SIZE; index++) {
				ReturnStatistics thisStatistics = get(index);
				ReturnStatistics otherStatistics = otherStats.get(index);
				if (thisStatistics == null) {
					set(index, otherStatistics);
				} else {
					LOGGER.error("This market type: " + thisStatistics.getMarketType() +
							" doesn't match other: " + otherStatistics.getMarketType());
				}
			}
			return true;
		}
		LOGGER.error("Collection was not of type ReturnStatisticsSet: " +  (otherCollection instanceof ReturnStatisticsSet)
				+ "\nor did not have matching investment horizon with: " + mInvestmentHorizonInMonths);
		return false;
	}
}
