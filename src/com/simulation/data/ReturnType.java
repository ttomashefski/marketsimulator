package com.simulation.data;

public enum ReturnType {
	MARKET_RETURN,
	REALIZED_RETURN,
	TOTAL_RETURN,
	AVERAGE_PER_TRADE_RETURN;
	
}
