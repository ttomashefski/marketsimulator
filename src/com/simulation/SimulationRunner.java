package com.simulation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.models.PriceDataPoint;
import com.models.ReportDate;
import com.models.broker.IBroker;
import com.models.broker.OpenPriceTriggerDCABroker;
import com.models.brokerfactory.BrokerFactoryBase;
import com.models.brokerfactory.IBrokerFactory;
import com.models.brokerfactory.IBrokerFactoryException;
import com.models.brokerfactory.OpenPriceTriggerCWDCABrokerFactory;
import com.simulation.data.DataAggregationRunnable;
import com.simulation.data.DataAnalysisRunnable;
import com.simulation.data.Database;
import com.simulation.data.ReturnStatistics;
import com.simulation.data.ReturnStatisticsSet;
import com.threading.MonitorThread;
import com.threading.PoolRejectionHandler;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.write.Number;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class SimulationRunner {
	private static final Logger LOGGER = LogManager.getLogger(SimulationRunner.class);
	private static final long TWICE_THE_SQUARE_OF_STDEV = 884500; //because we square a value x100 we needed to multiply this by 10000
	private static final long HISTORICAL_AVERAGE_CAPE = 1675;
	
	private Map<String, List<PriceDataPoint>> mMonthWithYearToPricePoint; //i.e. 10-2016 -> PriceDataPoint
	private Map<String, Double> mCapeRatioMap;
	private Date mEndingDate;
	private int mTotalRowCount;
	
	private static int MAX_SIMULATION_COUNT = 200;
	
	public SimulationRunner() {
		mMonthWithYearToPricePoint = new HashMap<String, List<PriceDataPoint>>();
		mCapeRatioMap = new HashMap<String, Double>();
		mEndingDate = null;
		mTotalRowCount = 0;
	}
	
	private void readCapeRatioData(WritableSheet sheet) throws RowsExceededException, WriteException {
		Cell cell = null;
		int row = 8;
		int dateColumn = 0;
		int capeRatioColumn = 10;
		
		do {
			cell = sheet.getCell(dateColumn, row);
			Cell cell1 = sheet.getCell(capeRatioColumn, row);

			String monthId = null;
			
			// Get the date
			String dateString = cell.getContents();
			//LOGGER.debug("Parsing: " + dateString);
			if (dateString.indexOf(".") == -1) {
				row++;
				continue;
			}
			int year = Integer.parseInt(dateString.substring(0, dateString.indexOf(".")));
			String subString = dateString.substring(1 + dateString.indexOf("."));
			if (subString.startsWith("1") && subString.length() == 1) {
				subString += "0";
			}
			int month = Integer.parseInt(subString);
			monthId = month + "-" + year;
			
			// Get the cape
			String contents = cell1.getContents();
			boolean successParsing = true;
			double capeRatio = 0;
			long currentCapeRatio = 0;
			try {
				capeRatio = Double.parseDouble(contents);
				currentCapeRatio = (long) (capeRatio);
			} catch (NumberFormatException e) {
				successParsing = false;
			}
			if (successParsing) {
				//long deviance = currentCapeRatio - HISTORICAL_AVERAGE_CAPE;
				//LOGGER.debug("Year-month: " + year + "-" + month + " Cape - average: " + currentCapeRatio + " deviance: " + deviance);
				//double value = (long) Math.pow(deviance, 2);		
				//LOGGER.debug("val: " + value);
				//value = 1 - Math.exp(-value / TWICE_THE_SQUARE_OF_STDEV); //I should put these in a pre calc table
				double value = 1 - (capeRatio / 33.50);
				LOGGER.debug("val: " + value);
				value = Double.max(0, value);
				LOGGER.debug("val max: " + value);
				Number number = new Number(capeRatioColumn + 1, row, value);
				sheet.addCell(number);
				mCapeRatioMap.put(monthId, value);
			} else {
				System.out.println("Second cell is not a number! " + cell.getContents());
			}			
			row++;
		} while (row < sheet.getRows() && cell.getContents() != null && !cell.getContents().isEmpty());
	}
	
	private PriceDataPoint readPriceData(Sheet sheet) {
		Cell cell = null;
		int row = 0;
		int dateColumn = 0;
		int openPriceColumn = 1;
		PriceDataPoint currentPriceDataPoint = null;
		PriceDataPoint startingDataPoint = null;
		
		do {
			cell = sheet.getCell(dateColumn, row);
			Cell cell1 = sheet.getCell(openPriceColumn, row);

			Date date = null;
			int openPrice = 0;
			String monthId = null;
			
			// Get the date
			if (cell.getType() == CellType.DATE) {
				DateCell dateCell = (DateCell) cell;

				//DateCell seems to be a day off...
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateCell.getDate());
				cal.add(Calendar.HOUR, 24);
				date = cal.getTime();
				mEndingDate = date;
				
				cal.setTime(date);
				int month = cal.get(Calendar.MONTH);
				int year = cal.get(Calendar.YEAR);
				monthId = month + "-" + year;
			} else {
				System.out.println("First cell is not a date! " + cell.getContents());
			}
			
			// Get the opening price
			String contents = cell1.getContents();
			boolean successParsing = true;
			double openPriceDouble = 0;
			try {
				openPriceDouble = Double.parseDouble(contents);
			} catch (NumberFormatException e) {
				successParsing = false;
			}
			if (successParsing) {
				openPrice = (int) (openPriceDouble * 100);
			} else {
				System.out.println("Second cell is not a number! " + cell.getContents());
			}
			
			if (monthId != null && successParsing) {
				// Create the price point
				PriceDataPoint nextPriceDataPoint = new PriceDataPoint(date, openPrice);
				if (currentPriceDataPoint != null) {
					currentPriceDataPoint.setNext(nextPriceDataPoint);
				}
				currentPriceDataPoint = nextPriceDataPoint;
				if (startingDataPoint == null) {
					startingDataPoint = currentPriceDataPoint;
				}
				
				List<PriceDataPoint> priceDataPointsForMonth = null;
				if (mMonthWithYearToPricePoint.containsKey(monthId)) {
					priceDataPointsForMonth = mMonthWithYearToPricePoint.get(monthId);
				} else {
					priceDataPointsForMonth = new ArrayList<PriceDataPoint>();
					mMonthWithYearToPricePoint.put(monthId, priceDataPointsForMonth);
				}
				priceDataPointsForMonth.add(currentPriceDataPoint);
			}
			
			row++;
		} while (row < sheet.getRows() && cell.getContents() != null && !cell.getContents().isEmpty());
		return startingDataPoint;
	}
	
	private IBrokerFactory createBrokerFactory(Date startDate, List<Integer> investmentHorizons) {
		Stack<ReportDate> reportDates = new Stack<ReportDate>(); //TODO clean this up
		Calendar cal = Calendar.getInstance();
		Iterator<Integer> listIterator = investmentHorizons.iterator();
		while (listIterator.hasNext()) {
			Integer horizonInMonths = listIterator.next();
			if (horizonInMonths != Integer.MAX_VALUE) {
				System.out.println("Setting start date: " + startDate);
				cal.setTime(startDate);
				cal.add(Calendar.MONTH, horizonInMonths);
			} else if (mEndingDate != null) {
				cal.setTime(mEndingDate);
			} // else may not report - this is kind of sloppy TODO
			Date reportDate = new Date(cal.getTime().getTime());
			LOGGER.debug("Reporting Date: " + reportDate);
			reportDates.push(new ReportDate(reportDate, horizonInMonths));
		}
		LOGGER.debug("FUPCUCUCSUIU: " + mCapeRatioMap);
		return new OpenPriceTriggerCWDCABrokerFactory(1000000, reportDates, -100, 100, 
				mCapeRatioMap);
		/*
		return new BrokerFactoryBase(reportDates) {

			@Override
			public IBroker createBroker() {
				return new OpenPriceTriggerDCABroker(1000000, mInvestmentReportingDates, -100, 100);
			}
			
		};
		*/
	}
	
	/**
	 * Executes the stock market strategy simulation. Spawns worker threads to simulate the market from
	 * specific starting dates in history providing a broker to execute a specific trading strategy. 
	 * Spawns data aggregators to aggregate the data reported by the brokers
	 * and then performs data anaylsis on the complete data set.
	 * 
	 * @throws IBrokerFactoryException
	 */
	private void executeSimulation() throws IBrokerFactoryException {
		try {
			Workbook workbook = Workbook.getWorkbook(new File("D:/Documents/Investments/2017/Historical_Prices_Daily.xls"));
			Sheet sheet = workbook.getSheet(0);
			mTotalRowCount = sheet.getRows();
			int columns = sheet.getColumns();
			System.out.println("Rows, Columns: "+mTotalRowCount+","+columns);
			PriceDataPoint startingPricePoint = readPriceData(sheet);
			
			Workbook workbookCape = Workbook.getWorkbook(new File("D:/Documents/Investments/2017/Historical_CAPE.xls"));
			WritableWorkbook copy = Workbook.createWorkbook(new File("D:/Documents/Investments/2017/Historical_CAPE_copy.xls"), workbookCape);
			WritableSheet sheetCape = copy.getSheet(2);
			readCapeRatioData(sheetCape);
			copy.write();
			copy.close();
			
			for (String key : mCapeRatioMap.keySet()) {
				LOGGER.debug("Weight Factor for Key: " + key + " Value: " + mCapeRatioMap.get(key));
			}
			
			Integer minimumHorizonInMonths = 1;
			List<Integer> mInvestmentHorizons = new ArrayList<Integer>();
			//mInvestmentHorizons.add(Integer.MAX_VALUE);
			mInvestmentHorizons.add(120); //24 months
			mInvestmentHorizons.add(12); //12 months
			mInvestmentHorizons.add(minimumHorizonInMonths);
//			mInvestmentHorizons.push(3);
//			mInvestmentHorizons.push(6);
//			mInvestmentHorizons.push(12);
//			mInvestmentHorizons.push(24);
//			mInvestmentHorizons.push(60);
//			mInvestmentHorizons.push(120);
			
			PoolRejectionHandler rejectionHandler = new PoolRejectionHandler();
			ThreadFactory threadFactory = Executors.defaultThreadFactory();
			ThreadPoolExecutor executorPool = new ThreadPoolExecutor(8, 8, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), threadFactory, rejectionHandler);
			
			//start the monitoring thread
			MonitorThread monitor = new MonitorThread(executorPool, 3);
			Thread monitorThread = new Thread(monitor);
			monitorThread.start();
			
			//submit work to the thread pool
			if (mMonthWithYearToPricePoint == null) {
				System.out.println("Failed to load data...");
			} else {
				Calendar cal = Calendar.getInstance();
				cal.setTime(startingPricePoint.getDate());
				cal.add(Calendar.MONTH, minimumHorizonInMonths);
				Date earliestHorizonDate = cal.getTime(); 
				int simulationCount = 0;
				int spreadPerSimulation = 20; //20 days (roughly 20 trading days per month)
				int numberOfStartingPoints = Integer.min(mTotalRowCount / spreadPerSimulation, MAX_SIMULATION_COUNT);
				int totalReportCount = numberOfStartingPoints * mInvestmentHorizons.size();

				LOGGER.info("Running Simulations... Total Report Count: " + totalReportCount);
				Database.PERFORMANCE_REPORT_DATABASE.setQueueReadyCount(1);
				//Database.FINAL_RETURN_STATISTICS_DATABASE.setQueueReadyCount(mInvestmentHorizons.size());
				SimulationTracker.getInstance().setNumberOfSimulationsRunning(Integer.min(numberOfStartingPoints, MAX_SIMULATION_COUNT));
				while (simulationCount < MAX_SIMULATION_COUNT
						&& startingPricePoint != null 
						&& earliestHorizonDate != null 
						&& earliestHorizonDate.compareTo(mEndingDate) < 0) {
					LOGGER.debug("Running Simulation: " + simulationCount);
					IBrokerFactory brokerFactory = createBrokerFactory(startingPricePoint.getDate(), mInvestmentHorizons);
				    executorPool.execute(new MarketSimulationRunnable(startingPricePoint, brokerFactory));
					
					for (int i=0; i < spreadPerSimulation && startingPricePoint.hasNext(); i++) {
						startingPricePoint = startingPricePoint.getNext();
					}
					cal.setTime(startingPricePoint.getDate());
					cal.add(Calendar.MONTH, minimumHorizonInMonths);
					earliestHorizonDate = cal.getTime();
					simulationCount++;
				}
				
				// Spawn Data Aggregators
				executorPool.execute(new DataAggregationRunnable());
				
				// Spawn Data Analyzer
				executorPool.execute(new DataAnalysisRunnable());
				
				// Wait for the results
				Iterator<Integer> iterator = mInvestmentHorizons.iterator();
				Set<ReturnStatisticsSet> finalResults = new TreeSet<ReturnStatisticsSet>();
				while (iterator.hasNext()) {
					iterator.next();
					LOGGER.debug("GET NEXT FOR ANALYSIS");
					Collection<ReturnStatistics> result = Database.FINAL_RETURN_STATISTICS_DATABASE.getNextCollectionForAnalysis(null);
					if (result instanceof ReturnStatisticsSet) {
						LOGGER.debug("Adding to final results: " + result);
						finalResults.add((ReturnStatisticsSet) result);
					} else {
						LOGGER.error("result not instance of ReturnStatisticsSet");
					}
				}
				
				//temp
				LOGGER.info("\n\n*******************************************************************"
						+ "\n*******************************************************************"
						+ "\n*******************************************************************"
						+ "\nFinal Results:");
				for (ReturnStatisticsSet result : finalResults) {
					LOGGER.info(result.toString());
				}
			}
	
			Thread.sleep(30000);
			
			//shut down the pool
			executorPool.shutdown();
			
			//shut down the monitor thread
			Thread.sleep(5000);
			monitor.shutdown(); 
		} catch (BiffException e) {
			System.out.println("Biff error?");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Thread Interrupted");
			e.printStackTrace();
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public static void main(String[] args) {
		try {
			new SimulationRunner().executeSimulation();
		} catch (IBrokerFactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
