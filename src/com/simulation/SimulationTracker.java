package com.simulation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.simulation.data.Database;

public class SimulationTracker {
	private static final Logger LOGGER = LogManager.getLogger(SimulationTracker.class);
	
	private static SimulationTracker gInstance = null;
	
	public static SimulationTracker getInstance() {
		if (gInstance == null) {
			gInstance = new SimulationTracker();
		}
		return gInstance;
	}
	
	private Map<Integer, Integer> mHorizonToReportCountMap; //maps and investment horizon in months 
														 //to the number of reports filed by brokers
	private Set<Integer> mAggregatedResultsReady;
	private Set<Integer> mAggregatedResultsDelayReady;
	private boolean mAggregatedResultInProgress;
	private int mTotalNumberOfSimulations;
	private int mNumberOfMarketSimulationsComplete;
	private int mNumberOfFinalReturnStatisticsComplete;
	
	private SimulationTracker() {
		mHorizonToReportCountMap = new HashMap<Integer, Integer>();
		mAggregatedResultsReady = new HashSet<Integer>();
		mAggregatedResultsDelayReady = new HashSet<Integer>();
		mAggregatedResultInProgress = false;
		mTotalNumberOfSimulations = Integer.MAX_VALUE;
		mNumberOfMarketSimulationsComplete = 0;
		mNumberOfFinalReturnStatisticsComplete = 0;
	}
	
	public synchronized void setNumberOfSimulationsRunning(int totalNumberOfSimulations) {
		LOGGER.info("Total Number Of Simulation Threads: " + totalNumberOfSimulations);
		mTotalNumberOfSimulations = totalNumberOfSimulations;
		mNumberOfMarketSimulationsComplete = 0;
		mNumberOfFinalReturnStatisticsComplete = 0;
	}
	
	/**
	 * Increments the reports filed count for a investment horizon
	 * @param investmentHorizonInMonths
	 * @param simulationsComplete 
	 */
	public synchronized void incrementReportCountFor(int investmentHorizonInMonths, boolean simulationsComplete) {
		int count = 1;
		if (mHorizonToReportCountMap.get(investmentHorizonInMonths) != null) {
			count += mHorizonToReportCountMap.get(investmentHorizonInMonths);
		}
		mHorizonToReportCountMap.put(investmentHorizonInMonths, count);
		
		LOGGER.debug("Current report count for " + investmentHorizonInMonths + " is " + count);
		LOGGER.debug("Total simulations is: " + mTotalNumberOfSimulations);
//		if (count == mTotalNumberOfSimulations && !mAggregatedResultInProgress) {
//			LOGGER.debug("Adding to Ready");
//			mAggregatedResultsReady.add(investmentHorizonInMonths);
//		} else 
		if (count == mTotalNumberOfSimulations) {
			LOGGER.debug("Adding to Delay Ready");
			mAggregatedResultsDelayReady.add(investmentHorizonInMonths);
		}
	}
	
	/**
	 * Attempts to add the aggregated results for the provided investment horizon
	 * to the ready queue but must first check if all performance reports are complete
	 * for the given horizon.
	 * @param investmentHorizonInMonths
	 * @param lastPass 
	 */
	public synchronized void aggregatedResultsReady(int investmentHorizonInMonths, boolean lastPass) {
//		if (lastPass && mAggregatedResultsDelayReady.contains(investmentHorizonInMonths)) {
//			LOGGER.debug("aggregatedResultsReady: " + investmentHorizonInMonths);
//			Database.REPORT_STATISTICS_DATABASE.addToReadyQueue(investmentHorizonInMonths);
//			mAggregatedResultsReady.remove(investmentHorizonInMonths);
//			mAggregatedResultsDelayReady.remove(investmentHorizonInMonths);
//			mHorizonToReportCountMap.remove(investmentHorizonInMonths);
//		} 
		if (mAggregatedResultsReady.contains(investmentHorizonInMonths)) {
			LOGGER.debug("aggregatedResultsReady: " + investmentHorizonInMonths);
			Database.REPORT_STATISTICS_DATABASE.addToReadyQueue(investmentHorizonInMonths);
			mAggregatedResultsReady.remove(investmentHorizonInMonths);
			mHorizonToReportCountMap.remove(investmentHorizonInMonths);
		} else if (mAggregatedResultsDelayReady.contains(investmentHorizonInMonths)) {
			LOGGER.debug("Moving " + investmentHorizonInMonths + " from delayed to ready");
			mAggregatedResultsReady.add(investmentHorizonInMonths);
			mAggregatedResultsDelayReady.remove(investmentHorizonInMonths);
		} else {
			LOGGER.debug("Aggregated Results Not Ready Yet");
		}
	}
	
	public synchronized void allAggregatedResultsReady() {
		for (Integer investmentHorizonInMonths : mAggregatedResultsReady) {
			LOGGER.debug("aggregatedResultsReady: " + investmentHorizonInMonths);
			Database.REPORT_STATISTICS_DATABASE.addToReadyQueue(investmentHorizonInMonths);
		}
		for (Integer investmentHorizonInMonths : mAggregatedResultsDelayReady) {
			LOGGER.debug("aggregatedResultsReady: " + investmentHorizonInMonths);
			Database.REPORT_STATISTICS_DATABASE.addToReadyQueue(investmentHorizonInMonths);
		}
	}
	
	public synchronized void setAggregatedResultsInProgress(boolean aggregatedResultsInProgress) {
		LOGGER.debug("setAggregatedResultsInProgress: " + aggregatedResultsInProgress);
		mAggregatedResultInProgress = aggregatedResultsInProgress;
	}
	
	public synchronized void incrementFinalReturnStatisticsComplete(int investmentHorizonInMonths) {
		mNumberOfFinalReturnStatisticsComplete++;
		//TODO - report progress
	}
	
	public synchronized boolean isMarketSimulationComplete() {
		return mNumberOfMarketSimulationsComplete == mTotalNumberOfSimulations;
	}
	
	public synchronized void marketSimulationComplete() {
		mNumberOfMarketSimulationsComplete++;
//		if (mNumberOfMarketSimulationsComplete == mTotalNumberOfSimulations && !mAggregatedResultInProgress) {
//			for (Integer investmentHorizonInMonths : mHorizonToReportCount.keySet()) {
//				Database.REPORT_STATISTICS_DATABASE.addToReadyQueue(investmentHorizonInMonths);
//			}
//			mHorizonToReportCount.clear();
//		} else 
		if (mNumberOfMarketSimulationsComplete == mTotalNumberOfSimulations) {
			LOGGER.debug("SIMULATIONS COMPLETE");
			for (Integer investmentHorizonInMonths : mHorizonToReportCountMap.keySet()) {
				mAggregatedResultsReady.add(investmentHorizonInMonths);
				Database.PERFORMANCE_REPORT_DATABASE.notifyReadyForAnalysis();
			}
		}
	}
}
