package com.simulation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.models.PriceDataPoint;
import com.models.broker.IBroker;
import com.models.broker.IBrokerException;
import com.models.brokerfactory.IBrokerFactory;
import com.models.brokerfactory.IBrokerFactoryException;

public class MarketSimulationRunnable implements Runnable {	
	private static final Logger LOGGER = LogManager.getLogger(MarketSimulationRunnable.class);
	
	private PriceDataPoint mStartingPricePoint;
	private IBrokerFactory mBrokerFactory;
	private IBroker mBroker;
	
	public MarketSimulationRunnable(PriceDataPoint startingPricePoint, IBrokerFactory brokerFactory) throws IBrokerFactoryException {
		mStartingPricePoint = startingPricePoint;
		mBrokerFactory = brokerFactory;
	}

	@Override
	public void run() {
		try {
			mBroker = mBrokerFactory.createBroker();
			if (mBroker == null || mStartingPricePoint == null) {
				System.out.println("Unable to run simulation due to null value for broker or starting price point");
			}
			LOGGER.debug("Running simulation for " + mStartingPricePoint.getDate());
			PriceDataPoint currentPricePoint = mStartingPricePoint; //we always start our strategy the next day
			mBroker.setCurrentPricePoint(currentPricePoint);
			while(currentPricePoint != null) {
				mBroker.setCurrentPricePoint(currentPricePoint);
				currentPricePoint = currentPricePoint.getNext();
				//Thread.sleep(2);
				
			}
			SimulationTracker.getInstance().marketSimulationComplete();
		} catch (IBrokerException e) {
			e.printStackTrace();
		} catch (IBrokerFactoryException e) {
			e.printStackTrace();
		}
		
	}

}
