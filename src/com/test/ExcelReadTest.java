/**
 * 
 */
package com.test;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import jxl.*;
import jxl.read.biff.BiffException;
/**
 * @author Tony
 *
 */
public class ExcelReadTest {
	
	private int buyPoint = -1;
	private int sellPoint = 1;
	
	private Map<Integer, List<Integer>> mMonthToDays;
	
	private void readPriceData(Sheet sheet) {
		Cell cell = sheet.getCell(0, 0);
		if (cell.getType() == CellType.DATE) {
			DateCell dateCell = (DateCell) cell;
			Date date = dateCell.getDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int month = cal.get(Calendar.MONTH);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Workbook workbook = Workbook.getWorkbook(new File("D:/Documents/Investments/2017/Historical_Prices_Daily.xls"));
			Sheet sheet0 = workbook.getSheet(0);
			int rows = sheet0.getRows();
			int columns = sheet0.getColumns();
			System.out.println("Rows,Columns: "+rows+","+columns);
			System.out.println("First cell: "+sheet0.getCell(1, 0).getContents());
		} catch (BiffException e) {
			System.out.println("Biff error?");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}
		

	}

}
