package com.reporting;

public enum MarketType {
	BULL_MARKET(1000), //10% up
	BEAR_MARKET(-1000), //10% down
	FLAT_MARKET_OR_UNKNOWN(0);
	
	private int mPercentChange; //0.01% represented as 1
	
	MarketType(int percentChange) {
		mPercentChange = percentChange;
	}
	
	public int getDefinition() {
		return mPercentChange;
	}
	
	public static MarketType getMarketTypeFor(int percentChange) {		
		if (percentChange >= BULL_MARKET.getDefinition()) {
			return BULL_MARKET;
		} else if (percentChange <= BEAR_MARKET.getDefinition()) {
			return BEAR_MARKET;
		} else {
			return FLAT_MARKET_OR_UNKNOWN;
		}
	}
}
