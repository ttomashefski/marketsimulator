package com.reporting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

public class PerformanceReportDatabase {
	
	private static PerformanceReportDatabase gInstance = null;
	
	public static PerformanceReportDatabase getInstance() {
		if (gInstance == null) {
			gInstance = new PerformanceReportDatabase();
		}
		return gInstance;
	}
	
	private Map<String, Set<PerformanceReport>> mDatabaseIndexedByMonth;
	private Map<Integer, Set<PerformanceReport>> mDatabaseIndexedByHorizon;
	private Queue<Integer> mReadyForAnalysis;
	private int mRecordCount;
	private int mNumberOfExpectedRecords;
	
	private PerformanceReportDatabase() {
		mDatabaseIndexedByMonth = new HashMap<String, Set<PerformanceReport>>();
		mDatabaseIndexedByHorizon = new HashMap<Integer, Set<PerformanceReport>>();
		mReadyForAnalysis = new LinkedBlockingQueue<Integer>();
		mRecordCount = 0;
		mNumberOfExpectedRecords = 0;
	}
	
	public void setCompletion(int numberOfHorizons, int numberOfStartingPoints) {
		mNumberOfExpectedRecords = numberOfHorizons * numberOfStartingPoints;
	}
	
	public synchronized void markReadyForAnalysis(Integer key) {
		mReadyForAnalysis.add(key);
	}
	
	public synchronized void insertPerformanceReport(String key, PerformanceReport value) {
		mRecordCount++;
		if (mNumberOfExpectedRecords != 0) {
			System.out.println("Progress: " + (mRecordCount / mNumberOfExpectedRecords) + "% Complete.");
		} else {
			System.out.println("Expecting 0 Records?");
		}
		if (mDatabaseIndexedByMonth.containsKey(key)) {
			mDatabaseIndexedByMonth.get(key).add(value);
			insertIntoHorizonIndex(value);
		} else {
			Set<PerformanceReport> reports = new TreeSet<PerformanceReport>();
			reports.add(value);
			mDatabaseIndexedByMonth.put(key, reports);
			insertIntoHorizonIndex(value);
		}
	}
	
	public synchronized List<PerformanceReport> getNextPerformanceListForAnalysis() {
		List<PerformanceReport> result = null;
		if (mReadyForAnalysis.peek() != null) {
			Integer key = mReadyForAnalysis.poll();
			if (mDatabaseIndexedByHorizon.containsKey(key)) {
				Set<PerformanceReport> reports = mDatabaseIndexedByHorizon.get(key);
				result = new ArrayList<PerformanceReport>();
				for(PerformanceReport report : reports) {
					result.add(report.deepCopy());
				}
			} else {
				System.out.println("Database does not contain key");
			}
		} else {
			System.out.println("No reports ready for analysis");
		}
		return result;
	}
	
	private void insertIntoHorizonIndex(PerformanceReport value) {
		Integer key = value.getInvestmentHorizonInMonths();
		if (mDatabaseIndexedByHorizon.containsKey(key)) {
			mDatabaseIndexedByHorizon.get(key).add(value);
		} else {
			Set<PerformanceReport> reports = new TreeSet<PerformanceReport>();
			reports.add(value);
			mDatabaseIndexedByHorizon.put(key, reports);
		}
		
	}
	
}
