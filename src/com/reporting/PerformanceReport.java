package com.reporting;

import java.util.Date;

import com.simulation.data.ReturnType;

/**
 * PerformanceReport contains a collection of data about how a portfolio has performed in a particular
 * investment horizon.
 * 
 * <Immutable Object>
 * @author Tony
 */
public class PerformanceReport implements Comparable<PerformanceReport> {
	private final Date mStartingDate;
	private final Date mReportingDate;
	
	private final MarketType mMarketType;
	private final int mInvestmentHorizonInMonths;
	
	private final int mMarketReturn;
	private final int mPortfolioRealizedReturn;
	private final int mPortfolioTotalReturn;
	
	private PerformanceReport(PerformanceReportBuilder builder) {
		mStartingDate = builder.mStartingDate;
		mReportingDate = builder.mReportingDate;
		mMarketType = builder.mMarketType;
		mInvestmentHorizonInMonths = builder.mInvestmentHorizonInMonths;
		mMarketReturn = builder.mMarketReturn;
		mPortfolioRealizedReturn = builder.mPortfolioRealizedReturn;
		mPortfolioTotalReturn = builder.mPortfolioTotalReturn;
		
	}
	
	private PerformanceReport(Date startingDate, Date reportingDate, MarketType marketType, int investmentHorizonInMonths,
			int marketReturn, int portfolioRealizedReturn, int portfolioTotalReturn) {
		mStartingDate = new Date(startingDate.getTime());
		mReportingDate = new Date(reportingDate.getTime());
		mMarketType = marketType;
		mInvestmentHorizonInMonths = investmentHorizonInMonths;
		mMarketReturn = marketReturn;
		mPortfolioRealizedReturn = portfolioRealizedReturn;
		mPortfolioTotalReturn = portfolioTotalReturn;
		
	}

	public PerformanceReport deepCopy() {
		PerformanceReport copy = new PerformanceReport(mStartingDate, mReportingDate, mMarketType,
				mInvestmentHorizonInMonths, mMarketReturn, mPortfolioRealizedReturn, mPortfolioTotalReturn);
		return copy;
	}

	public MarketType getMarketType() {
		return mMarketType;
	}
	
	public Integer getInvestmentHorizonInMonths() {
		return mInvestmentHorizonInMonths;
	}
	
	public int getMarketReturn() {
		return mMarketReturn;
	}
	
	public int getPortfolioRealizedReturn() {
		return mPortfolioRealizedReturn;
	}	
	
	public int getPortfolioTotalReturn() {
		return mPortfolioTotalReturn;
	}
	
	public int getReturnForType(ReturnType type) {
		switch (type) {
		case MARKET_RETURN:
			return mMarketReturn;
		case REALIZED_RETURN:
			return mPortfolioRealizedReturn;
		case TOTAL_RETURN:
			return mPortfolioTotalReturn;
		case AVERAGE_PER_TRADE_RETURN: //TODO
		default:
			return 0;
		}
	}

	@Override
	public int compareTo(PerformanceReport other) {
		return mReportingDate.compareTo(other.mReportingDate);
	}
	
	@Override
	public String toString() {
		return "\nPerformance " + mMarketType + " Horizon: " + mInvestmentHorizonInMonths
				+ "\n\tMarket Return: " + mMarketReturn
				+ "\n\tPortfolio Realized Return: " + mPortfolioRealizedReturn
				+ "\n\tPortfolio Total Return: " + mPortfolioTotalReturn
				+ "\n\tfor the period: " + mStartingDate + " - " + mReportingDate;
	}

	/**
	 * 
	 * @author Tony
	 */
	public static class PerformanceReportBuilder {
		private Date mStartingDate;
		private Date mReportingDate;
		
		private MarketType mMarketType;
		private int mInvestmentHorizonInMonths;
		
		private int mMarketReturn;
		private int mPortfolioRealizedReturn;
		private int mPortfolioTotalReturn;

	    public PerformanceReportBuilder(Date startingDate, Date reportingDate) {
	    	this.mStartingDate = new Date(startingDate.getTime());
	    	this.mReportingDate = new Date(reportingDate.getTime());
	    }
	    
	    public PerformanceReportBuilder marketType(MarketType marketType) {
	    	this.mMarketType = marketType;
	    	return this;
	    }
	    
	    public PerformanceReportBuilder investmentHorizonInMonths(int investmentHorizonInMonths) {
	    	this.mInvestmentHorizonInMonths = investmentHorizonInMonths;
	    	return this;
	    }
	    
	    public PerformanceReportBuilder marketReturn(int marketReturn) {
	    	this.mMarketReturn = marketReturn;
	    	return this;
	    }
	    
	    public PerformanceReportBuilder portfolioRealizedReturn(int portfolioRealizedReturn) {
	    	this.mPortfolioRealizedReturn = portfolioRealizedReturn;
	    	return this;
	    }
	    
	    public PerformanceReportBuilder portfolioTotalReturn(int portfolioTotalReturn) {
	    	this.mPortfolioTotalReturn = portfolioTotalReturn;
	    	return this;
	    }

	    public PerformanceReport build() {
	    	return new PerformanceReport(this); //TODO - throw exception if incomplete?
	    }

	}
	
}
